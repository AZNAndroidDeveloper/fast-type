package uz.azn.homework

class WordsAndTexts {

    companion object{

        val wordList = arrayListOf<String>(
            "this",
            "context",
            "class",
            "interface",
            "abstract",
            "enum",
            "TextView",
            "ImageView",
            "MainActivity",
            "manifests",
            "icon",
            "drawable"
        )

        val textList =  arrayListOf<String>(
            "Android studio 4.1",
            "MainActivity : AppCompatActivity()",
            "addTextChangedListener",
            "setOnClickListener",
            "ConstraintLayout",
            "ActivityTextsBinding",
            "androidx.constraintlayout.widget.ConstraintLayout",
            "onChildTitleChanged",
            "OnDestroy",
            "afterTextChanged",
            "onTextChanged",
            "MutableList"

        )
    }
}