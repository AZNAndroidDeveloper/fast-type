package uz.azn.homework

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import uz.azn.homework.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)


        binding.textButton.setOnClickListener {
            val intent  = Intent(applicationContext,TextsActivity::class.java)
            intent.putExtra("text",true)
            startActivity(intent)

        }




        binding.wordButton.setOnClickListener {
            val intent = Intent(applicationContext,TextsActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onChildTitleChanged(childActivity: Activity?, title: CharSequence?) {
        super.onChildTitleChanged(childActivity, title)
    }
}